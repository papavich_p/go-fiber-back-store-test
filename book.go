package main

import (
	"fmt"
	"os"
	"strconv"
	"time"

	"github.com/gofiber/fiber/v2"

	"github.com/golang-jwt/jwt/v4"
)

type User struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

// สร้างข้อมูล member
var userMemeber = User{
	Email:    "papavich@gmail.com",
	Password: "12345",
}

func Login(c *fiber.Ctx) error {
	// เรียกใช้งาน struct ของ user เพื่อไว้ใช้เป็น instance สำหรับเก็บรับค่าที่จะส่งมา
	user := new(User)

	// รับค่าเข้ามาผ่าน bodyParser
	if err := c.BodyParser(user); err != nil {
		return c.Status(fiber.StatusBadRequest).SendString(err.Error())
	}

	// ถ้าไม่มีอะไรผิดปกติให้มาเช็คค่าที่รับเข้ามากับ userMember
	if user.Email != userMemeber.Email || user.Password != userMemeber.Password {
		return fiber.ErrUnauthorized
	}

	// สร้าง token
	// Create token
	token := jwt.New(jwt.SigningMethodHS256)

	// Set claims
	claims := token.Claims.(jwt.MapClaims)
	claims["email"] = "John Doe"
	claims["role"] = "admin"
	claims["exp"] = time.Now().Add(time.Hour * 72).Unix()

	// Generate encoded token
	t, err := token.SignedString([]byte(os.Getenv("SECRET_KEY")))
	if err != nil {
		return c.SendStatus(fiber.StatusInternalServerError)
	}

	// หากถูกต้องก็ return login successful
	return c.Status(fiber.StatusOK).JSON(fiber.Map{"message": "Login Successful", "token": t})

}

// middle
func middleCheck(c *fiber.Ctx) error {

	// เรียกใช้งานเวลาที่มีการเรียกใช้ฟังก์ชันนี้
	start := time.Now()

	fmt.Printf("URL = %s, Method = %s, Time = %s", c.OriginalURL(), c.Method(), start)

	return c.Next()

}

func getDotEnv(c *fiber.Ctx) error {

	// ดึงข้อมูล env
	mySecret := os.Getenv("SECRET_KEY")

	// ดัก mySecret ว่างเปล่าหรือไม่
	if mySecret == "" {
		mySecret = "default secret"
	}

	return c.JSON(fiber.Map{"message": mySecret})
}

func testHtml(c *fiber.Ctx) error {
	return c.Render("index", fiber.Map{
		"Title": "World",
	})
}

func uploadImage(c *fiber.Ctx) error {
	// Get the form data values
	file, err := c.FormFile("image")
	if err != nil {
		return c.Status(fiber.StatusBadRequest).SendString(err.Error())
	}

	// กำหนดตำแหน่งเก็บไฟล์
	err = c.SaveFile(file, "./uploads/"+file.Filename)

	if err != nil {
		return c.Status(fiber.StatusInternalServerError).SendString(err.Error())
	}

	return c.Status(fiber.StatusOK).SendString("Upload file complete")

}

func deleteBook(c *fiber.Ctx) error {
	bookId, err := strconv.Atoi(c.Params("id"))
	if err != nil {
		return c.Status(fiber.StatusBadRequest).SendString(err.Error())
	}

	for i, book := range books {
		if book.ID == bookId {
			//[1,2,3,4,5]
			// [1,2] + [4,5] = [1,2,4,5]
			books = append(books[:i], books[i+1:]...)
			// books[:i] หมายถึงเอาตัวตั้งแต่ 0 จนถึง ตัวก่อนหน้าก็คือ เช่น 3
			// จะได้ [1,2]

			// books[i+1:] เลือกเอาตัวถัดจากตัวที่เราต้องการไปจนถึงตัวท้ายสุด
			// append ไม่สามารถเอา slice มาต่อ slice ได้ดังนั้นเราต้อง
			// spear operator ... (books[i+1:]...)
			// ก็จะได้ความหมายประมาณนี้ [1,2] + [4,5] = [1,2,4,5]
			return c.Status(fiber.StatusOK).JSON(fiber.Map{"message": "delete success.", "data": books[i : i+1]})
		}
	}

	return c.Status(fiber.StatusBadRequest).SendString("Book Not Found")

}

func updateBook(c *fiber.Ctx) error {
	// รับค่า id มาจาก params ก่อน
	bookId, err := strconv.Atoi(c.Params("id"))
	if err != nil {
		return c.Status(fiber.StatusBadRequest).SendString(err.Error())
	}

	// รับค่าข้อมูลที่จะแก้ไขผ่าน body parser
	newBook := new(Book)

	// ถ้ารับเข้ามาแล้วพัง
	if err := c.BodyParser(newBook); err != nil {
		return c.Status(fiber.StatusBadRequest).SendString(err.Error())
	}

	// ค้นหา book ที่เราจะแก้ไขด้วย id
	for i, book := range books {
		if book.ID == bookId {
			books[i].Name = newBook.Name
			books[i].Author = newBook.Author
			fmt.Println(books)
			return c.Status(fiber.StatusOK).JSON(books[i])
		}
	}

	fmt.Println(books)
	//dev-papavich
	//master
	// ถ้าไม่เจอหนังสือ
	return c.Status(fiber.StatusBadRequest).SendString("Book Not Found")

}

func createBook(c *fiber.Ctx) error {
	// รับค่าผ่าน bodyParser
	// สร้าง book ที่เป็น instance  ของ struct Book ขึ้นมา
	// ซึ่งจะเป็นการจอง address ไว้ ดังนั้นตัว book นี้เลยเปรียบเสมือน pointer

	book := new(Book)

	// รับค่าเข้ามาผ่าน bodyParser และเอาไปเก็บไว้ที่ตัวแปร book
	// เนื่องจาก book ของเรานั้นเป็นการจอง address นั้นก็เท่ากับว่ามันมีค่าเป็น pointer จึงไม่จำเป็นต้องใส่ &book
	if err := c.BodyParser(book); err != nil {
		return c.Status(fiber.StatusBadRequest).SendString(err.Error())
	}

	// เอาข้อมูลรับเข้าไปต่อกับตัวข้อมูล book เนื่องจาก book มีค่าเป็น address เพื่อที่จะใช้ไปยังข้อมูลที่อยู่ตาม address ตอนนี้ก็
	// ให้ใส่ *book เข้าไปด้วย
	books = append(books, *book)

	fmt.Println("Books = ", books)

	return c.Status(fiber.StatusCreated).JSON(book)
}

func getBooks(c *fiber.Ctx) error {

	println("books", books) //books [2/2]0xc00008a000
	fmt.Printf("books slice = %v", books)
	return c.JSON(books)
}

func getBook(c *fiber.Ctx) error {

	// แปลง string เป็น int เนื่องจ่ากตัว books  ข้อมูลรับมาเป็น string

	id, err := strconv.Atoi(c.Params("id"))
	if err != nil {
		return c.Status(fiber.StatusBadRequest).SendString(err.Error())
	}

	// loop หาหนังสือ
	for _, book := range books {
		if book.ID == id {
			return c.Status(fiber.StatusOK).JSON(book)
		}
	}
	return c.Status(fiber.StatusBadRequest).SendString("Book Not Found EIEI")
}
