package main

import (
	"log"
	"os"

	"github.com/gofiber/fiber/v2"
	jwtware "github.com/gofiber/jwt/v2"
	"github.com/gofiber/template/html/v2"
	"github.com/joho/godotenv"
)

// add function 1
// Tracking 1
// papavich-1
// Karty 555
// ประกาศ struct เพื่อไว้สำหรับรับค่าข้อมูลหนังสือที่จะรับเข้ามา
// เนื่องจากเรายังไ่ใช้งาน database เราเลยทำการประกาศตัวแปร goble เหล่านี้ไว้ไปเรียกใช้งานได้จากทุก ๆ function
type Book struct {
	ID     int    `json:"id"`     // ตัวแปร id ใช้รับค่า id ของหนังสือ
	Name   string `json:"name"`   // ตัวแปร name ใช้รับค่าของชื่อหนังสือ
	Author string `json:"author"` // ตัวแปร author
}

// ประกาศ slice เพื่อไว้เป็น array เก็บตัวหนังสือ
// Test Gitlab
var books []Book

func main() {

	// เรียกใช้งาน view ที่เป็น  html
	// Initialize standard Go html template engine
	engine := html.New("./views", ".html")

	app := fiber.New(fiber.Config{
		Views: engine,
	})

	// คำสั่ง load env
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	// เพิ่มค่าเริ่มต้นให้กับ books slice
	books = append(books, Book{ID: 1, Name: "Book_1", Author: "Au_1"})
	books = append(books, Book{ID: 2, Name: "Book_2", Author: "Au_2"})

	// ทดลองใช้งาน middleware แบบต้องผ่านฟังก์ชันนี้ก่อนจะไปทำงาน api อะไรก็ตามแต่
	// app.Use(middleCheck)

	// login
	app.Post("/login", middleCheck, Login)

	// JWT Middleware
	app.Use(jwtware.New(jwtware.Config{
		SigningKey: []byte(os.Getenv("SECRET_KEY")),
	}))

	// หรือเรียกใช้เฉพาะบางฟังก์ชัน
	// api
	app.Get("/books", middleCheck, getBooks)
	app.Get("/books/:id", getBook)
	app.Post("/", createBook)
	app.Put("/books/:id", updateBook)
	app.Delete("/books/:id", deleteBook)
	app.Post("/upload", uploadImage)

	// html
	app.Get("test-html", testHtml)

	// get env
	app.Get("/SECRET", getDotEnv)

	app.Listen(":8080")
}
